package com.example.convertidor;

import androidx.appcompat.app.AppCompatActivity;
import android.app.Activity;
import android.os.Bundle;
import android.view.View;
import android.widget.Adapter;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {
    private Spinner spnConversiones;
    private Button btnCerrar;
    private TextView lblTotal;
    private EditText lblCantidad;
    private Button btnCalcular;
    private Button btnlimpiame;
    private long opcion;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        lblTotal=(TextView)findViewById(R.id.lblTotal);
        lblCantidad= (EditText) findViewById(R.id.lblCantidad);
        btnCalcular= (Button) findViewById(R.id.btnCalcular);
        btnlimpiame= (Button)findViewById(R.id.btnLimpiame);
        btnCerrar= (Button) findViewById(R.id.btnCerrar);
        spnConversiones =(Spinner) findViewById(R.id.spnConvertidor);

        final ArrayAdapter<String> adapter = new ArrayAdapter<>
                (MainActivity.this,android.R.layout.simple_list_item_1);
        spnConversiones.setAdapter(adapter);
        spnConversiones.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {


                opcion = adapterView.getItemIdAtPosition(i);
                lblTotal.setText("Total: ");
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });




        //BOTON CERRAR
        btnCerrar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        //BOTON LIMPIAR
        btnlimpiame.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                lblCantidad.setText("");
                lblTotal.setText("Total: ");
                spnConversiones.setAdapter(adapter);
                ArrayAdapter<String> Adaptador=new
                        ArrayAdapter<String>(MainActivity.this,android.R.layout.simple_expandable_list_item_1,getResources().getStringArray(R.array.Conversiones));
                spnConversiones.setAdapter(Adaptador);

            }
        });

        //boto pa calcualar aca bien perron
        btnCalcular.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //ciclo para que no truene el programa al hacer click con nada puesto
                if(lblCantidad.getText().toString().matches(""))
                    Toast.makeText(MainActivity.this,"falto capturar numero",
                            Toast.LENGTH_SHORT).show();
                else {
                    float resultado = 0.0f;
                    float cantidad = Float.parseFloat(lblCantidad.getText().toString());

                    switch ((int) opcion) {

                        case 0:
                            resultado = cantidad * 0.05f;
                            break;
                        case 1:
                            resultado = cantidad * 0.048f;
                            break;
                        case 2:
                            resultado = cantidad * 0.07f;
                            break;
                        case 3:
                            resultado = cantidad * 0.41f;
                            break;
                    }

                    lblTotal.setText("Total: " + String.valueOf(resultado));

                }

            }
        });

        ArrayAdapter<String> Adaptador=new
                ArrayAdapter<String>(MainActivity.this,android.R.layout.simple_expandable_list_item_1,getResources().getStringArray(R.array.Conversiones));
        spnConversiones.setAdapter(Adaptador);
        spnConversiones.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                                                        @Override
                                                        public void onItemSelected(AdapterView<?> adapterView, View view,
                                                                                   int i, long l) {
                                                            opcion=adapterView.getItemIdAtPosition(i);
                                                            lblTotal.setText("");
                                                        }
                                                        @Override
                                                        public void onNothingSelected(AdapterView<?> adapterView) {
                                                        }});}}